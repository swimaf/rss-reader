import React, {Component} from 'react';
import {Button, Jumbotron, Card} from 'react-bootstrap';
import Parser from 'rss-parser';
import "./Parser.css";

const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

class Reader extends Component {
    constructor(props) {
        super(props);
        this.state = {rss : {items : []}, select : 1, error: false, message : ""};
        this.url = props.url;
        this.parser = new Parser();
        this.handleChangePage = this.handleChangePage.bind(this);
    }
    componentDidMount() {
        this.loadRss(this.url);
    }

    componentWillReceiveProps(nextProps) {
        this.loadRss(nextProps.url);
    }


    handleChangePage(e) {
        let select = (this.state.select+e) % this.state.rss.items.length;
        if(select < 0 ) {
            select +=  this.state.rss.items.length;
        }
        this.setState({select: select});
    }

    timeout(ms, promise) {
        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                reject(new Error("timeout"))
            }, ms);
            promise.then(resolve, reject)
        })
    }

    loadRss(url) {
        this.setState({rss: {items: []}, error: false, message : ""});
        let data = window.localStorage.getItem("lastKnown_"+url);

        if((!navigator.onLine && data != null) || (navigator.onLine && data != null && (Date.now() - JSON.parse(data).time) < 600000)) {
            this.setState(({rss: JSON.parse(data).data, select: 0}));
        } else if(!navigator.onLine) {
             this.setState(({error: true, message : "Votre navigateur n'est pas connecté à internet"}));
        } else  {
            this.timeout( 10000, fetch(CORS_PROXY + url))
                .then((response) => response.text())
                .then((responseData) => this.parser.parseString(responseData))
                .then((rss) => {
                    this.setState(({rss: rss, select: 0}));
                    window.localStorage.setItem(
                        "lastKnown_"+url,
                        JSON.stringify({
                            data : rss,
                            time : Date.now()
                        })
                    );

                }).catch(() => {
                this.setState(({error: true, message : "Erreur lors du chargement de la source"}));
            });
        }


    }

    render() {
        return (
            <Jumbotron className="pt-3 vh-100">
                <div hidden={this.state.rss.title || this.state.error}>
                    <div className="vh-100 d-flex align-items-center justify-content-center">
                        <img src="./loading.svg"  alt="Loading"/>
                    </div>
                </div>
                <div hidden={!this.state.error}>
                    <div className="vh-100 d-flex align-items-center flex-column justify-content-center">
                        <img className="icon-center" src="./error.svg" alt="Erreur"/>
                        <p>{this.state.message}</p>
                    </div>
                </div>
                <div>
                    <div className="justify-content-between d-flex align-items-center">
                        <h1>{this.state.rss.title}</h1>
                        <div hidden={this.state.rss.items.length === 0}>
                            <Button className="mr-2" onClick={() => this.handleChangePage(-1)}>Précédent</Button>
                            <Button  onClick={() => this.handleChangePage(1)}>Suivant</Button>
                        </div>
                    </div>
                    {this.state.rss.items.length > 0 &&
                        <Card>
                            <Card.Header><a href={this.state.rss.items[this.state.select].link} rel="noopener noreferrer" target="_blank">{this.state.rss.items[this.state.select].title}</a></Card.Header>
                            <Card.Body>
                                <Card.Subtitle>{this.state.rss.items[this.state.select].pubDate}</Card.Subtitle>
                                <Card.Text dangerouslySetInnerHTML={{ __html: this.state.rss.items[this.state.select].content }}/>
                                <div className="d-flex justify-content-end">
                                    <Button variant="primary" target="_blank" href={this.state.rss.items[this.state.select].link}>Plus d'information</Button>
                                </div>
                            </Card.Body>
                        </Card>
                    }

                </div>
            </Jumbotron>
        );
    }

}

export default Reader;
