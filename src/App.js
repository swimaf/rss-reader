import React, { Component } from 'react';
import {Navbar, Button, Dropdown, ButtonGroup} from 'react-bootstrap';
import Reader from "./Parser";
import "./Parser.css";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url : "",
            sources : [
                {
                    name : "Programmez",
                    url : "https://www.programmez.com/rss.xml",
                },
                {
                    name : "Journal du net",
                    url : "https://www.journaldunet.com/iot/rss/"
                },
                {
                    name : "Le monde informatique",
                    url : "https://www.lemondeinformatique.fr/fluxrss/thematique/logiciel/rss.xml"
                },
                {
                    name : "Developpez",
                    url : "https://www.developpez.com/index/rss"
                }
            ]
        };
        this.handleSource = this.handleSource.bind(this);
    }

    handleSource(e) {
        let url = this.state.sources.filter(value => value.name === e.target.innerHTML)[0].url;
        this.setState({url : url});
    }

    render() {
    return (
      <div className="App">
        <header className="App-header">
            <Navbar bg="light" expand="lg" className="justify-content-between">
                <Navbar.Brand href="/">React RSS</Navbar.Brand>
                <Dropdown as={ButtonGroup} drop="left">
                    <Button variant="success">Source</Button>
                    <Dropdown.Toggle split variant="success" aria-hidden="true" />
                    <Dropdown.Menu >
                        {this.state.sources.map(e => (
                            <Dropdown.Item onClick={this.handleSource}>{e.name}</Dropdown.Item>
                        ))}
                    </Dropdown.Menu>
                </Dropdown>
            </Navbar>
        </header>
          <div hidden={this.state.url !== ""} className="">
              <div className="vh-100 justify-content-center flex-column d-flex align-items-center">
                  <img className="icon-center" src="./rss.svg" alt="Rss"/>
                  <p className="text-center">Choisir une source</p>
              </div>
          </div>
          {this.state.url !== "" &&
              <div>
                  <Reader url={this.state.url}/>
              </div>
          }
      </div>
    );
  }
}

export default App;
